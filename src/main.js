import Vue from 'vue'
import Antd from 'ant-design-vue'
import App from './app'
import 'ant-design-vue/dist/antd.css'

Vue.use(Antd)

new Vue({
  render: h => h(App),
}).$mount('#app')

if (process.env.NODE_ENV !== 'production' && module.hot) {
  module.hot.accept()
}
